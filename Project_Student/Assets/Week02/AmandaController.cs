﻿using UnityEngine;
using System.Collections;

public class AmandaController : MonoBehaviour {

	public Animator AmandaAnimator;

	// Use this for initialization
	void Start() {
		
	}

	// Update is called once per frame
	void Update() {
		if (Input.GetKey (KeyCode.W)) {
			AmandaAnimator.SetFloat ("Speed", 1);
		}
		else {
			AmandaAnimator.SetFloat ("Speed", 0);
		}

		if (Input.GetKey (KeyCode.LeftShift)) {
			AmandaAnimator.SetBool ("Sprint", true);
		} else {
			AmandaAnimator.SetBool ("Sprint", false);
		}

		if (Input.GetKeyDown (KeyCode.Space)) {
			AmandaAnimator.SetTrigger ("Jump");
		}


		if (Input.GetKeyDown (KeyCode.E)) {
			AmandaAnimator.SetTrigger ("Rolling");
		}

		//if (Input.GetKeyDown (KeyCode.D)) {
		//	SkeletonAnimator.SetBool ("Attack" , true);
		//}

		//if (Input.GetKeyUp (KeyCode.D)) {
		//	SkeletonAnimator.SetBool ("Attack" , false	);
		//}

	
	}
}
