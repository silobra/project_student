using UnityEngine;
using System.Collections;

namespace IGD246.Week02 {

    public class SkeletonController : MonoBehaviour {

		public Animator SkeletonAnimator;
		public int MyHP;

        // Use this for initialization
        void Start() {
			MyHP = 100;
        }

        // Update is called once per frame
        void Update() {
			if (Input.GetKey (KeyCode.UpArrow)) {
				SkeletonAnimator.SetFloat ("Speed", 1);
			}
			else {
				SkeletonAnimator.SetFloat ("Speed", 0);
			}

			if (Input.GetKey (KeyCode.LeftShift)) {
				SkeletonAnimator.SetBool ("IsSprint", true);
			} else {
				SkeletonAnimator.SetBool ("IsSprint", false);
			}

			if (Input.GetKeyDown (KeyCode.Space)) {
				MyHP = 0;
			}
			SkeletonAnimator.SetInteger ("Hp", MyHP);


			if (Input.GetKeyDown (KeyCode.D)) {
				SkeletonAnimator.SetTrigger ("AttackTrigger");
				}

			//if (Input.GetKeyDown (KeyCode.D)) {
			//	SkeletonAnimator.SetBool ("Attack" , true);
			//}

			//if (Input.GetKeyUp (KeyCode.D)) {
			//	SkeletonAnimator.SetBool ("Attack" , false	);
			//}

        }
    }


}