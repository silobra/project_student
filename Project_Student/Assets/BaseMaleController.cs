﻿using UnityEngine;
using System.Collections;

public class BaseMaleController : MonoBehaviour {


	public Animator baseMaleAnimator;

	// Use this for initialization
	void Start() {

	}

	// Update is called once per frame
	void Update() {
		if (Input.GetKey (KeyCode.W)) {
			baseMaleAnimator.SetFloat ("Speed", 1);
		}
		else {
			baseMaleAnimator.SetFloat ("Speed", 0);
		}

		if (Input.GetKey (KeyCode.LeftShift)) {
			baseMaleAnimator.SetBool ("Sprint", true);
		} else {
			baseMaleAnimator.SetBool ("Sprint", false);
		}

		if (Input.GetKeyDown (KeyCode.Space)) {
			baseMaleAnimator.SetTrigger ("Jump");
		}


		if (Input.GetKeyDown (KeyCode.E)) {
			baseMaleAnimator.SetTrigger ("Rolling");
		}

		//if (Input.GetKeyDown (KeyCode.D)) {
		//	SkeletonAnimator.SetBool ("Attack" , true);
		//}

		//if (Input.GetKeyUp (KeyCode.D)) {
		//	SkeletonAnimator.SetBool ("Attack" , false	);
		//}


	}
}
